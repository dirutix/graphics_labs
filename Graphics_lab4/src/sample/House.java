package sample;

import com.sun.j3d.utils.geometry.Box;
import com.sun.j3d.utils.geometry.Cylinder;
import com.sun.j3d.utils.geometry.GeometryInfo;
import com.sun.j3d.utils.geometry.NormalGenerator;
import com.sun.j3d.utils.universe.SimpleUniverse;

import javax.media.j3d.*;
import javax.swing.*;
import javax.vecmath.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

// Vector3f - float, Vector3d - double
public class House implements ActionListener {
    private float upperEyeLimit = 8.0f; // 5.0
    private float lowerEyeLimit = 5.0f; // 1.0
    private float farthestEyeLimit = 12.0f; // 6.0
    private float nearestEyeLimit = 10.0f; // 3.0

    private TransformGroup treeTransformGroup;
    private TransformGroup viewingTransformGroup;
    private Transform3D treeTransform3D = new Transform3D();
    private Transform3D viewingTransform = new Transform3D();
    private float angle = 0;
    private float eyeHeight;
    private float eyeDistance;
    private boolean descend = true;
    private boolean approaching = true;

    public static void main(String[] args) {
        new House();
    }

    private House() {
        Timer timer = new Timer(50, this);
        SimpleUniverse universe = new SimpleUniverse();

        viewingTransformGroup = universe.getViewingPlatform().getViewPlatformTransform();
        universe.addBranchGraph(createSceneGraph());

        eyeHeight = upperEyeLimit;
        eyeDistance = farthestEyeLimit;
        timer.start();
    }

    private BranchGroup createSceneGraph() {
        BranchGroup objRoot = new BranchGroup();

        treeTransformGroup = new TransformGroup();
        treeTransformGroup.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        buildHouse();
        objRoot.addChild(treeTransformGroup);

        Background background = new Background(new Color3f(0.9f, 0.9f, 0.9f)); // white color
        BoundingSphere sphere = new BoundingSphere(new Point3d(0,0,0), 100000);
        background.setApplicationBounds(sphere);
        objRoot.addChild(background);

        BoundingSphere bounds = new BoundingSphere(new Point3d(0.0, 0.0, 0.0),100.0);
        Color3f light1Color = new Color3f(1.0f, 0.5f, 0.4f);
        Vector3f light1Direction = new Vector3f(4.0f, -7.0f, -12.0f);
        DirectionalLight light1 = new DirectionalLight(light1Color, light1Direction);
        light1.setInfluencingBounds(bounds);
        objRoot.addChild(light1);

        Color3f ambientColor = new Color3f(1.0f, 1.0f, 1.0f);
        AmbientLight ambientLightNode = new AmbientLight(ambientColor);
        ambientLightNode.setInfluencingBounds(bounds);
        objRoot.addChild(ambientLightNode);
        return objRoot;
    }

    private void buildHouse() {
        //Facade
        var body = new Box(1.7f, 1.7f, 2.7f, Utils.getBodyAppearence());
        Transform3D bodyT = new Transform3D();
        bodyT.setTranslation(new Vector3f());
        bodyT.rotX(Math.PI / 2);
        TransformGroup bodyTG = new TransformGroup();
        bodyTG.setTransform(bodyT);
        bodyTG.addChild(body);

        //ROOF
        Point3f e = new Point3f(3.8f, 1.8f, 0.0f); // east
        Point3f s = new Point3f(0.0f, 1.8f, 3.8f); // south
        Point3f w = new Point3f(-3.8f, 1.8f, 0.0f); // west
        Point3f n = new Point3f(0.0f, 1.8f, -3.8f); // north
        Point3f t = new Point3f(0.0f, 3.721f, 0.0f); // top

        TriangleArray pyramidGeometry = new TriangleArray(18,
                TriangleArray.COORDINATES);
        pyramidGeometry.setCoordinate(0, e);
        pyramidGeometry.setCoordinate(1, t);
        pyramidGeometry.setCoordinate(2, s);

        pyramidGeometry.setCoordinate(3, s);
        pyramidGeometry.setCoordinate(4, t);
        pyramidGeometry.setCoordinate(5, w);

        pyramidGeometry.setCoordinate(6, w);
        pyramidGeometry.setCoordinate(7, t);
        pyramidGeometry.setCoordinate(8, n);

        pyramidGeometry.setCoordinate(9, n);
        pyramidGeometry.setCoordinate(10, t);
        pyramidGeometry.setCoordinate(11, e);

        pyramidGeometry.setCoordinate(12, e);
        pyramidGeometry.setCoordinate(13, s);
        pyramidGeometry.setCoordinate(14, w);

        pyramidGeometry.setCoordinate(15, w);
        pyramidGeometry.setCoordinate(16, n);
        pyramidGeometry.setCoordinate(17, e);
        GeometryInfo geometryInfo = new GeometryInfo(pyramidGeometry);
        NormalGenerator ng = new NormalGenerator();
        ng.generateNormals(geometryInfo);

        GeometryArray result = geometryInfo.getGeometryArray();
        Shape3D shape = new Shape3D(result, Utils.getRoofAppearence());

        Transform3D roof = new Transform3D();
        roof.setTranslation(new Vector3f(0, 1, 0));
        roof.rotY(Math.PI/4);
        TransformGroup roofTG = new TransformGroup();
        roofTG.setTransform(roof);
        roofTG.addChild(shape);

        bodyTG.addChild(roofTG);


        var door = new Box(0.4f, 1.3f, 0.4f, Utils.getDoorAppearence());
        Transform3D doorT = new Transform3D();
        doorT.setTranslation(new Vector3f(1.5f,-0.3f,0.5f));
        TransformGroup doorTG = new TransformGroup();
        doorTG.setTransform(doorT);
        doorTG.addChild(door);

        bodyTG.addChild(doorTG);

        var window = new Cylinder(0.5f, 0.8f);
        Transform3D windowT = new Transform3D();
        windowT.rotZ(Math.PI/2);
        windowT.setTranslation(new Vector3f(1.5f,0.3f,-0.7f));
        TransformGroup windowTG = new TransformGroup();
        windowTG.setTransform(windowT);
        windowTG.addChild(window);



        bodyTG.addChild(windowTG);

        var chimney = new Cylinder(0.3f, 2f);
        Transform3D chimneyT = new Transform3D();
        chimneyT.setTranslation(new Vector3f(0.5f,3f,-0.7f));
        TransformGroup chimneyTG = new TransformGroup();
        chimneyTG.setTransform(chimneyT);
        chimneyTG.addChild(chimney);

        bodyTG.addChild(chimneyTG);

        treeTransformGroup.addChild(bodyTG);

    }

    // ActionListener interface
    @Override
    public void actionPerformed(ActionEvent e) {
        float delta = 0.03f;

        // rotation of the castle
        treeTransform3D.rotZ(angle);
        treeTransformGroup.setTransform(treeTransform3D);
        angle += delta;

        // change of the camera position up and down within defined limits
        if (eyeHeight > upperEyeLimit){
            descend = true;
        }else if(eyeHeight < lowerEyeLimit){
            descend = false;
        }
        if (descend){
            eyeHeight -= delta;
        }else{
            eyeHeight += delta;
        }

        // change camera distance to the scene
        if (eyeDistance > farthestEyeLimit){
            approaching = true;
        }else if(eyeDistance < nearestEyeLimit){
            approaching = false;
        }
        if (approaching){
            eyeDistance -= delta;
        }else{
            eyeDistance += delta;
        }

        Point3d eye = new Point3d(eyeDistance, eyeDistance, eyeHeight); // spectator's eye
        Point3d center = new Point3d(.0f, .0f ,0.1f); // sight target
        Vector3d up = new Vector3d(.0f, .0f, 1.0f);; // the camera frustum
        viewingTransform.lookAt(eye, center, up);
        viewingTransform.invert();
        viewingTransformGroup.setTransform(viewingTransform);
    }
}
