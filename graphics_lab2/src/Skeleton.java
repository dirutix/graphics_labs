import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Ellipse2D;
import java.awt.geom.GeneralPath;

import javax.swing.*;

public class Skeleton extends  JPanel implements ActionListener {

    private static int maxWidth;
    private static int maxHeight;

    Timer timer;

    private double angle = 0;

    private double tx = 340;
    private double ty = 140;
    private double radius = 130;

    public Skeleton(){
        timer = new Timer(10, this);
        timer.start();
    }

    public void paint(Graphics g) {

        // Оскільки Java2D є надбудовою над старішою бібліотекою, необхідно робити це приведення
        Graphics2D g2d = (Graphics2D) g;
        RenderingHints rh = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        rh.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);

        g2d.setRenderingHints(rh);
        // Далі йде безпосередньо малювання. Для прикладу намалюємо такий рядок
        g2d.drawString("Привіт, Java 2D!", 50, 50);

        g2d.setBackground(Color.MAGENTA);
        g2d.clearRect(0, 0, maxWidth, maxHeight);

        g2d.setColor(Color.RED);
        g2d.fillRect(110, 350, 195, 90);

        g2d.setColor(Color.BLUE);
        g2d.fill(new Ellipse2D.Double(110, 60, 80, 380));

        g2d.setColor(Color.GREEN);
        g2d.fillRect(110, 60, 390, 90);

        GradientPaint gp = new GradientPaint(20, 30, Color.PINK, 40, 40, Color.BLACK, true);
        g2d.setPaint(gp);
        g2d.fill(new Ellipse2D.Double(420, 60, 80, 380));

        g2d.setColor(Color.RED);
        double points[][] = {{305, 350}, {500, 350}, {500, 440}, {305, 440}};
        GeneralPath rect = new GeneralPath();
        rect.moveTo(points[0][0], points[0][1]);
        for (int k = 1; k < points.length; k++)
            rect.lineTo(points[k][0], points[k][1]);
        rect.closePath();
        g2d.fill(rect);

        BasicStroke bs = new BasicStroke(12, BasicStroke.CAP_ROUND, BasicStroke.JOIN_BEVEL);
        g2d.setStroke(bs);
        g2d.setColor(Color.YELLOW);
        g2d.drawRect(540, 60, 380, 380);


        //Animetion
        double points2[][] = { { 0, 85 }, { 75, 75 }, { 100, 10 }, { 125, 75 },
                { 200, 85 }, { 150, 125 }, { 160, 190 }, { 100, 150 },
                { 40, 190 }, { 50, 125 }, { 0, 85 } };
        GeneralPath star = new GeneralPath();
        star.moveTo(points2[0][0]/2, points2[0][1]/2);

        g2d.translate(680, 210);

        g2d.translate(tx, ty);

        for (int k = 1; k < points2.length; k++)
            star.lineTo(points2[k][0]/2, points2[k][1]/2);
        star.closePath();

        g2d.rotate(2 * angle, star.getBounds2D().getCenterX(), star.getBounds2D().getCenterY());

        g2d.setColor(Color.BLACK);
        g2d.fill(star);
    }

    public static void main(String[] args) {

        // Створюємо нове графічне вікно (формочка). Параметр конструктора - заголовок вікна.
        JFrame frame = new JFrame("Привіт, Java 2D!");
        // Визначаємо поведінку програми при закритті вікна (ЛКМ на "хрестик")
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        // Визначаємо розмір вікна
        frame.setSize(1000, 600);
        // Якщо позиція прив'язана до null, вікно з'явиться в центрі екрану
        frame.setLocationRelativeTo(null);
        // Забороняємо змінювати розміри вікна
        frame.setResizable(false);
        // Додаємо до вікна панель, що і описується даним класом
        // Зауважте, що точка входу в програму - метод main, може бути й в іншому класі
        frame.add(new Skeleton());
        // Показуємо форму. Важливо додати всі компоненти на форму до того, як зробити її видимою.
        frame.setVisible(true);
        Dimension size = frame.getSize();
        Insets insets = frame.getInsets();
        maxWidth =  size.width - insets.left - insets.right - 1;
        maxHeight =  size.height - insets.top - insets.bottom - 1;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        angle += 0.01;
        tx = radius * Math.cos(angle);
        ty = radius * Math.sin(angle);

        repaint();
    }
}
