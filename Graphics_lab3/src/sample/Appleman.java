package sample;

import javafx.animation.*;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.awt.geom.RoundRectangle2D;

public class Appleman extends Application{
    public static void main(String[] args) { launch(args);}

    @Override
    public void start(Stage primaryStage){
        Group root = new Group();
        Scene scene = new Scene(root, 1200, 600);

        //Leaf2
        MoveTo ml2 = new MoveTo(285, 232);
        QuadCurveTo qf1 = new QuadCurveTo(306, 214, 313, 175);
        QuadCurveTo qf2 = new QuadCurveTo(294, 185, 268, 180);
        QuadCurveTo qf3 = new QuadCurveTo(240, 175, 207, 203);
        Path leaf2 = new Path();
        leaf2.setStrokeWidth(2);
        leaf2.setStroke(Color.BLACK);
        leaf2.setFill(Color.DARKGREEN);
        leaf2.getElements().addAll(ml2, qf1, qf2, qf3);
        root.getChildren().add(leaf2);

        //Stick
        MoveTo ms1 = new MoveTo(203, 204);
        QuadCurveTo qs1= new QuadCurveTo(199, 187, 192, 180);
        QuadCurveTo qs2 = new QuadCurveTo(202, 176, 209, 180);
        QuadCurveTo qs3 = new QuadCurveTo(211, 187, 211, 200);
        Path stick = new Path();
        stick.setStrokeWidth(2);
        stick.setStroke(Color.BLACK);
        stick.setFill(Color.BROWN);
        stick.getElements().addAll(ms1, qs1, qs2, qs3);
        root.getChildren().add(stick);

        //Body
        Ellipse big = new Ellipse(226.6, 285.5,80, 80);
        big.setStroke(Color.RED);
        big.setFill(Color.RED);
        big.setStrokeWidth(4);
        root.getChildren().add(big);

        Ellipse s1 = new Ellipse(211, 343, 31, 31);
        s1.setStroke(Color.RED);
        s1.setFill(Color.RED);
        s1.setStrokeWidth(4);
        root.getChildren().add(s1);

        Ellipse s2 = new Ellipse(245, 341.5, 33, 33);
        s2.setStroke(Color.RED);
        s2.setFill(Color.RED);
        s2.setStrokeWidth(4);
        root.getChildren().add(s2);

        MoveTo mt1 = new MoveTo(211, 374);
        QuadCurveTo qt1 = new QuadCurveTo(170.3, 380, 147.8, 299.3);
        Path apple = new Path();
        apple.setStrokeWidth(2);
        apple.setStroke(Color.RED);
        apple.setFill(Color.RED);
        apple.getElements().addAll(mt1, qt1);
        root.getChildren().add(apple);

        MoveTo lt1 = new MoveTo(306, 293.7);
        QuadCurveTo qt2 = new QuadCurveTo(285, 380, 245, 374);
        Path apple2 = new Path();
        apple2.setStrokeWidth(2);
        apple2.setStroke(Color.RED);
        apple2.setFill(Color.RED);
        apple2.getElements().addAll(lt1, qt2);
        root.getChildren().add(apple2);

        //Leaf 1
        MoveTo mtl1 = new MoveTo(215, 195);
        QuadCurveTo qtl1 = new QuadCurveTo(177, 211, 186, 239);
        QuadCurveTo qtl2 = new QuadCurveTo(205, 251, 229, 255);
        QuadCurveTo ql3 = new QuadCurveTo(246, 260, 260, 270);
        QuadCurveTo ql4 = new QuadCurveTo(270, 230, 253, 203);
        QuadCurveTo ql5 = new QuadCurveTo(226, 188, 215, 195);
        Path leaf1 = new Path();
        leaf1.setStrokeWidth(2);
        leaf1.setStroke(Color.BLACK);
        leaf1.setFill(Color.GREEN);
        leaf1.getElements().addAll(mtl1, qtl1, qtl2, ql3, ql4, ql5);
        root.getChildren().add(leaf1);


        QuadCurveTo ql6 = new QuadCurveTo(246, 220, 260, 270);
        Path leaf1stroke = new Path();
        leaf1stroke.setStrokeWidth(2);
        leaf1stroke.setStroke(Color.BLACK);
        leaf1stroke.setFill(Color.GREEN);
        leaf1stroke.getElements().addAll(mtl1, ql6);
        root.getChildren().add(leaf1stroke);

        //Eye 1
        Ellipse e1 = new Ellipse(215, 317, 7.5, 10);
        e1.setStroke(Color.BLACK);
        e1.setFill(Color.WHITE);
        e1.setStrokeWidth(1);
        root.getChildren().add(e1);

        Ellipse e2 = new Ellipse(248, 317, 7.5, 10);
        e2.setStroke(Color.BLACK);
        e2.setFill(Color.WHITE);
        e2.setStrokeWidth(1);
        root.getChildren().add(e2);

        //Pupils
        Ellipse p1 = new Ellipse(217, 320, 4, 4);
        p1.setStroke(Color.BLACK);
        p1.setFill(Color.BLACK);
        p1.setStrokeWidth(1);
        root.getChildren().add(p1);
        Ellipse p2 = new Ellipse(250, 320, 4, 4);
        p2.setStroke(Color.BLACK);
        p2.setFill(Color.BLACK);
        p2.setStrokeWidth(1);
        root.getChildren().add(p2);

        //Torch
        MoveTo tm1 = new MoveTo(230, 327);
        LineTo tl1 = new LineTo(230, 331);
        QuadCurveTo tq1 = new QuadCurveTo(239, 337.5, 230, 339);
        Path torch = new Path();
        torch.setStrokeWidth(2);
        torch.setStroke(Color.BLACK);
        torch.getElements().addAll(tm1, tl1, tq1);
        root.getChildren().add(torch);

        //Smile
        MoveTo smm1 = new MoveTo(215 ,345);
        QuadCurveTo smq1 = new QuadCurveTo(230, 355, 245, 345);
        Path smile = new Path();
        smile.setStrokeWidth(2);
        smile.setStroke(Color.BLACK);
        smile.getElements().addAll(smm1, smq1);
        root.getChildren().add(smile);

        //Hand 1
        MoveTo hm1 = new MoveTo(306, 275);
        QuadCurveTo qth1 = new QuadCurveTo(315, 285, 315, 295);
        QuadCurveTo qth2 = new QuadCurveTo(311, 307, 304, 316);
        QuadCurveTo qth3 = new QuadCurveTo(304, 325, 307, 330);
        QuadCurveTo qth4 = new QuadCurveTo(315, 322, 320, 323);
        QuadCurveTo qth5 = new QuadCurveTo(316, 330, 322, 334);
        QuadCurveTo qth6 = new QuadCurveTo(314, 334, 315, 334);
        MoveTo hm2 = new MoveTo(307, 330);
        Path hand1 = new Path();
        hand1.setStrokeWidth(2);
        hand1.setStroke(Color.BLACK);
        hand1.getElements().addAll(hm1, qth1, qth2, qth3, qth4, hm2, qth5, hm2, qth6);
        root.getChildren().add(hand1);

        //Hand 2
        MoveTo hm3 = new MoveTo(147, 287);
        QuadCurveTo qth7 = new QuadCurveTo(160, 300, 145, 322);
        QuadCurveTo qth8 = new QuadCurveTo(141, 337, 160, 340);
        QuadCurveTo qth9 = new QuadCurveTo(161, 335, 172, 332);
        MoveTo hm4 = new MoveTo(160, 340);
        QuadCurveTo qth10 = new QuadCurveTo(167, 338, 174, 339);
        LineTo lh1 = new LineTo(176, 345);
        Path hand2 = new Path();
        hand2.setStrokeWidth(2);
        hand2.setStroke(Color.BLACK);
        hand2.getElements().addAll(hm3, qth7, qth8, qth9, hm4, qth10, hm4, lh1);
        root.getChildren().add(hand2);

        // Animation
        int cycleCount = 2;
        int time = 2000;

        ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(time), root);
        scaleTransition.setToX(20);
        scaleTransition.setToY(20);
        scaleTransition.setAutoReverse(true);

        RotateTransition rotateTransition = new RotateTransition(Duration.millis(time), root);
        rotateTransition.setByAngle(360f);
        rotateTransition.setCycleCount(cycleCount);
        rotateTransition.setAutoReverse(true);

        TranslateTransition translateTransition = new TranslateTransition(Duration.millis(time), root);
        translateTransition.setFromX(150);
        translateTransition.setToX(400);
        translateTransition.setFromY(100);
        translateTransition.setToY(50);
        translateTransition.setCycleCount(cycleCount + 1);
        translateTransition.setAutoReverse(true);

        ScaleTransition scaleTransition2 = new ScaleTransition(Duration.millis(time), root);
        scaleTransition2.setToX(0.1);
        scaleTransition2.setToY(0.1);
        scaleTransition2.setCycleCount(cycleCount);
        scaleTransition2.setAutoReverse(true);

        ParallelTransition parallelTransition = new ParallelTransition();
        parallelTransition.getChildren().addAll(
                rotateTransition,
                scaleTransition,
                scaleTransition2,
                translateTransition
        );
        parallelTransition.setCycleCount(Timeline.INDEFINITE);
        parallelTransition.play();
//        // End of animation

        //End
        primaryStage.setResizable(false);
        primaryStage.setTitle("Lab 3");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
