package sample;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class Main extends Application {

    final int WIDTH = 600;
    final int HEIGHT = 500;

    @Override
    public void start(Stage primaryStage) throws Exception{
        Group root = new Group();
        Scene scene = new Scene(root, WIDTH, HEIGHT);
        scene.setFill(Color.MAGENTA);

        Rectangle rR1 = new Rectangle(110, 350, 195, 90);
        rR1.setFill(Color.RED);
        root.getChildren().add(rR1);

        Ellipse ellB = new Ellipse(150, 250, 40, 190);
        ellB.setFill(Color.BLUE);
        root.getChildren().add(ellB);

        Rectangle rG = new Rectangle(110, 60, 390, 90);
        rG.setFill(Color.GREEN);
        root.getChildren().add(rG);

        Ellipse ellY = new Ellipse(460, 250, 40, 190);
        ellY.setFill(Color.YELLOW);
        root.getChildren().add(ellY);

        Rectangle rR2 = new Rectangle(305, 350, 195, 90);
        rR2.setFill(Color.RED);
        root.getChildren().add(rR2);

        primaryStage.setScene(scene);
        primaryStage.setTitle("Graphics_lab1");
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
